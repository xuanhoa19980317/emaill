define(function (require) {
	"use strict";
	var $ = require('jquery'),
		_ = require('underscore'),
		Gonrin = require('gonrin');
	return [
		{
			"collectionName": "email", 
			"route": "email/collection(/:id)",
			"$ref": "app/view/Email/CollectionView",
		},
		{
			"collectionName": "email",
			"route": "email/model(/:id)",
			"$ref": "app/view/Email/ModelView",
		},

	];

});


