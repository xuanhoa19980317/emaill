define(function (require) {
    "use strict";
    var $                   = require('jquery'),
        _                   = require('underscore'),
        Gonrin				= require('gonrin');
    
    var template 			= require('text!app/view/Email/tpl/collection.html'),
    	schema 				= require('json!schema/EmailSchema.json');

    return Gonrin.CollectionDialogView.extend({
    	template : template,
    	modelSchema	: schema,
    	urlPrefix: "/api/v1/",
    	collectionName: "email",
    	textField: "ten",
    	valueField: "id",
    	tools : [
    	    {
    	    	name: "defaultgr",
    	    	type: "group",
    	    	groupClass: "toolbar-group",
    	    	buttons: [
					{
		    	    	name: "select",
		    	    	type: "button",
		    	    	buttonClass: "btn-success btn-sm",
		    	    	label: "TRANSLATE:SELECT",
		    	    	command: function(){
		    	    		var self = this;
		    	    		self.trigger("onSelected");
		    	    		self.close();
		    	    	}
		    	    },
    	    	]
    	    },
    	],
    	uiControl:{
    		fields: [
				{ 
	    	    	field: "maungvien",label:"Mã Ứng Viên"
				 },
				 { 
	    	    	field: "tenungvien",label:"Tên Ứng Viên"
				 },
				 { 
	    	    	field: "cvfile",label:"CV"
				 },
				 { 
	    	    	field: "applyfor",label:"ApplyFor"
				 },
				 { 
	    	    	field: "stage",label:"Stage"
				 },	
				 { 
	    	    	field: "hrnote",label:"HrNote"
				 },
				 { 
	    	    	field: "pool",label:"Vị Trí Ứng Tuyển"
				 },
				 { 
	    	    	field: "whocare",label:"Care"
				 },
				 { 
	    	    	field: "time",label:"start_time"
				 },
				 { 
	    	    	field: "dob",label:"dob"
				 },	
				 { 
	    	    	field: "week",label:"week"
				 },
				 { 
	    	    	field: "source",label:"source"
				 },	
		    ],
		    onRowClick: function(event){
	    		this.uiControl.selectedItems = event.selectedItems;
	    		
	    	},
    	},
    	render:function(){
    		var self= this;
    		
    		self.applyBindings();
    		
    		return this;
    	},
    	
    });

});