define(function (require) {
    "use strict";
    var $                   = require('jquery'),
        _                   = require('underscore'),
        Gonrin				= require('gonrin');
    
    var template 			= require('text!app/view/Email/tpl/collection.html');
    var	schema 				= require('json!schema/EmailSchema.json');
    
    return Gonrin.CollectionView.extend({
    	template : template,
    	modelSchema	: schema,
    	urlPrefix: "/api/v1/",
		collectionName: "email",
		tools: [
			{
				name: "default",
				type: "group",
				groupClass: "toolbar-group",
				buttons: [
					{
						name: "create",
						type: "button",
						buttonClass: "btn-success btn-sm",
						label: "Tạo mới",
						command: function(){
							var self = this;
							var path = self.collectionName + '/model';
							self.getApp().getRouter().navigate(path);
						}
					},
					
				]
			},
		],
    	uiControl:{
    		fields: [
				{ 
	    	    	field: "maungvien",label:"Mã Ứng Viên"
				 },
				 { 
	    	    	field: "tenungvien",label:"Tên Ứng Viên"
				 },
				 { 
	    	    	field: "cvfile",label:"CV"
				 },
				 { 
	    	    	field: "applyfor",label:"ApplyFor"
				 },
				 { 
	    	    	field: "stage",label:"Stage"
				 },	
				 { 
	    	    	field: "hrnote",label:"HrNote"
				 },
				 { 
	    	    	field: "pool",label:"Vị Trí Ứng Tuyển"
				 },
				 { 
	    	    	field: "whocare",label:"Care"
				 },
				 { 
	    	    	field: "time",label:"start_time"
				 },
				 { 
	    	    	field: "Dob",label:"dob"
				 },	
				 { 
	    	    	field: "week",label:"week"
				 },
				 { 
	    	    	field: "source",label:"source"
				 },		 
		     ],
		     onRowClick: function(event){
		    	if(event.rowId){
		        		var path = this.collectionName + '/model?id='+ event.rowId;
		        		this.getApp().getRouter().navigate(path);
		        }
		    	
		    }
    	},
	    render:function(){
	    	 this.applyBindings();
	    	 return this;
    	},
    });
});
